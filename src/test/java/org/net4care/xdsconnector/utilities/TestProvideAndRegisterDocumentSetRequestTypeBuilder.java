package org.net4care.xdsconnector.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.net4care.xdsconnector.Constants.COID;
import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.Utilities.CodedValue.CodedValueBuilder;
import org.net4care.xdsconnector.Utilities.ProvideAndRegisterDocumentSetRequestTypeBuilder;
import org.net4care.xdsconnector.service.ProvideAndRegisterDocumentSetRequestType;
import org.springframework.util.StringUtils;

public class TestProvideAndRegisterDocumentSetRequestTypeBuilder {

  private CodedValue healthcareFacilityTypeCode = new CodedValueBuilder()
      .setCode(COID.DK.FacilityCode)
      .setCodeSystem(COID.DK.FacilityCodeSystem)
      .setDisplayName(COID.DK.facilityTypeCode2DisplayName(COID.DK.FacilityCode))
      .build();
  private CodedValue practiceSettingCode = new CodedValueBuilder()
      .setCode("408443003")
      .setCodeSystem(COID.DK.PracticeSettingCodeSystem)
      .setDisplayName("almen medicin")
      .build();

  @Test
  public void buildProvideAndRegisterDocumentRequest() throws Exception {
    // Arrange
    String cda = getCDA("examples/Ex1-Weight_measurement.xml");
    ProvideAndRegisterDocumentSetRequestTypeBuilder builder = new ProvideAndRegisterDocumentSetRequestTypeBuilder();
    // Act
    ProvideAndRegisterDocumentSetRequestType request = builder.buildProvideAndRegisterDocumentRequest(
      cda, healthcareFacilityTypeCode, practiceSettingCode).getRequest();
    // Assert
    assertNotNull(request);
    assertNotNull(builder.getRequestPayload());
  }

  @Test
  public void buildProvideAndRegisterDocumentRequestForMultipleDocs() throws Exception {
    // Arrange
    List<String> cdaDocuments = Arrays.asList(
      getCDA("examples/Ex1-Weight_measurement.xml"),
      getCDA("examples/PHMRNancy.xml"),
      getCDA("examples/QRDLungInformationNeedsQuestionaire.xml"));
    ProvideAndRegisterDocumentSetRequestTypeBuilder builder = new ProvideAndRegisterDocumentSetRequestTypeBuilder();
    // Act
    ProvideAndRegisterDocumentSetRequestType request = builder.buildProvideAndRegisterDocumentRequest(
      cdaDocuments, healthcareFacilityTypeCode, practiceSettingCode).getRequest();
    // Assert
    assertNotNull(request);
    assertNotNull(builder.getRequestPayload());
    assertNotNull(request.getSubmitObjectsRequest());
    assertNotNull(request.getSubmitObjectsRequest().getRegistryObjectList());
    int registrationObjects = 
          1 /* submission set (registration package) */
        + 1 /* classification node */ 
        + 2 * cdaDocuments.size(); /* document entry + association (to submission set) */
    assertEquals(registrationObjects, request.getSubmitObjectsRequest().getRegistryObjectList().getIdentifiable().size());
    assertEquals(cdaDocuments.size(), request.getDocument().size());
  }

  private String getCDA(String path) throws IOException, URISyntaxException {
    java.net.URL url = getClass().getClassLoader().getResource(path);
    List<String> lines = Files.readAllLines(Paths.get(url.toURI()), Charset.forName("UTF-8"));
    return StringUtils.collectionToDelimitedString(lines, "\n");
  }
}
