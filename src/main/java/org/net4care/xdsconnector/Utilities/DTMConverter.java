package org.net4care.xdsconnector.Utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DTMConverter {
  private DateFormat dtmDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
  private DateFormat timeZoneDateFormat = new SimpleDateFormat("yyyyMMddHHmmssZ");
  
  public DTMConverter() {
    dtmDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  public String convert(Date date) {
    return dtmDateFormat.format(date);
  }
  
  public String convert(String dateTimeString) throws ParseException {
    return convert(timeZoneDateFormat.parse(dateTimeString));
  }
  
}
