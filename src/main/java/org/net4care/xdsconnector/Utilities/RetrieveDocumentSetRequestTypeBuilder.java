package org.net4care.xdsconnector.Utilities;

import javax.xml.bind.JAXBElement;

import org.net4care.xdsconnector.service.ObjectFactory;
import org.net4care.xdsconnector.service.RetrieveDocumentSetRequestType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetRequestType.DocumentRequest;

public class RetrieveDocumentSetRequestTypeBuilder {

  private RetrieveDocumentSetRequestType request;
  private JAXBElement<RetrieveDocumentSetRequestType> requestPayload;

  public RetrieveDocumentSetRequestTypeBuilder() {
    request = new ObjectFactory().createRetrieveDocumentSetRequestType();
    requestPayload = new ObjectFactory().createRetrieveDocumentSetRequest(request);
  }

  /**
   * Adds a DocumentRequest consisting of the given parameters of which only the homeCommunityId can
   * be null or empty
   * @param documentUniqueId
   * @param repositoryUniqueId
   * @param homeCommunityId
   * @return
   */
  public RetrieveDocumentSetRequestTypeBuilder addDocumentRequest(
    String documentUniqueId,
    String repositoryUniqueId,
    String homeCommunityId) {
    DocumentRequest documentRequest = new DocumentRequest();
    documentRequest.setDocumentUniqueId(documentUniqueId);
    documentRequest.setRepositoryUniqueId(repositoryUniqueId);
    documentRequest.setHomeCommunityId(homeCommunityId);
    request.getDocumentRequest().add(documentRequest);
    return this;
  }
  
  public JAXBElement<RetrieveDocumentSetRequestType> getRequestPayload() {
    return requestPayload;
  }
  
}
