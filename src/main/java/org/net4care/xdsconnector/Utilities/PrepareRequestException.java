package org.net4care.xdsconnector.Utilities;

public class PrepareRequestException extends Exception {

  private static final long serialVersionUID = -7474634475997204216L;

  public PrepareRequestException(String message) {
    super(message);
  }

  public PrepareRequestException(Throwable throwable) {
    super(throwable);
  }

  public PrepareRequestException(String message, Throwable throwable) {
    super(message, throwable);
  }

}


